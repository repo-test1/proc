import {getLogger} from '../logger';
import {config} from '../config';
import './some_module';

const log = getLogger('index');

async function main() {
    log.info(config.headline);

    log.info(config.end);
}

main().catch((e) => {
    log.error(e.message);
});
